//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Neil McGuiness on 26/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include <cmath>
#include <iostream>



MidiMessage:: MidiMessage(int defaultMidiNoteVal, int defaultVelocity, int defaultChannelNums)

{
    number = defaultMidiNoteVal;
    velocity = defaultVelocity;
    channelNums = defaultChannelNums;
    
}

MidiMessage:: ~MidiMessage()

{
    std::cout << "Destructor\n";
}

void MidiMessage::setNoteNumber(int value)

{
    if (value > 0)
    
    {
        number = value;

    }
    
    else
    {
        std::cout << "Error! Invalid note number.\n";
    }
}

int MidiMessage:: getNoteNumber() const


{
    return number;
}

float MidiMessage:: getMidiNoteInHertz() const

{
    return 440 * pow(2, (number-69) / 12.0);
}

void MidiMessage::setVelocity(int value)

{
    if (value > 0 && value < 128)
        
    {
        velocity = value;
        
    }
    
    else
        
    {
        std::cout << "Error! Invalid velocity value.\n";
    }
}

int MidiMessage:: getVelocity() const


{
    return velocity;
}

void MidiMessage::setChannelNum(int value)

{

if (value > 0)

{
    channelNums = value;
    
}

else
{
    std::cout << "Error! Invalid number of channels.\n";
}
    
}

int MidiMessage:: getChannelNum() const


{
    return channelNums;
}

float MidiMessage:: getFloatVelocity() const

{
    return velocity / 127.0;
}


