//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.h"


// Instance of my MidiMessage class.
MidiMessage note(60, 127, 2);

int main (int argc, const char* argv[])
{
    int userNote;
    int userVel;
    int userChannels;
    
    // Getting value from User with a prompt.
    std::cout << "Please enter a Midi note number! \n";
    // Obtaining value.
    std::cin >> userNote;
    //Feeding value into function.
    
    std::cout << "Please enter a velocity value. \n";
    
    std::cin >> userVel;
    
    std::cout << "Please enter a the number of channels. \n";
    
    std::cin >> userChannels;

    
    note.setNoteNumber(userNote);
    note.setVelocity(userVel);
    note.setChannelNum(userChannels);
    
    std::cout << "Midi note is : " << note.getNoteNumber() << std::endl << "Frequency value of note in Hz : " << note.getMidiNoteInHertz() << std::endl << "Velocity : " << note.getVelocity() << std:: endl << "Amplitude : " << note.getFloatVelocity() << std::endl << "Number of channels :" << note.getChannelNum() << std::endl;
    
    
    
   
    
    
    return 0;
}


