//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Neil McGuiness on 26/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_h
#define MidiMessage_h

class MidiMessage

{
public:
    MidiMessage(int defaultMidiNoteVal, int defaultVelocity, int defaultChannelNums);
    
    ~MidiMessage();
    
    void setNoteNumber (int value); // Mutator
    
    int getNoteNumber() const; // Accessor
    
    void setVelocity (int value); // Mutator
    
    int getVelocity() const; // Accessor
    
    void setChannelNum(int value); // Mutator
    
    int getChannelNum() const; // Accessor
    
    float getMidiNoteInHertz() const; // Accessor
    
    float getFloatVelocity() const; // Accessor
    
    
    
private:
    
    int number, channelNums, velocity;
    
    
    
};


#endif /* MidiMessage_h */
